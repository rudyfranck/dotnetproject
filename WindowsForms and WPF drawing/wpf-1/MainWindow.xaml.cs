﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace wpf_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random rnd;
        DispatcherTimer timer;

        public MainWindow()
        {
            InitializeComponent();
            rnd = new Random();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 2000);
            timer.Tick += Timer_Tick;
            timer.Start();

            this.KeyDown += grid1_KeyDown;
             
            createRectangles(1);
           
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
           // createRectangles(1);
        }

        private void createRectangles(int val)
        {
            int rectWidth = 100;
            int rectHeight = 50;
            const int border = 20;
            const int caption = 50;
            for (int i = 0; i < val; i++)
            {
                int x = rnd.Next((int)this.Width - rectWidth- border);
                int y = rnd.Next((int)this.Height - rectHeight - caption);

                Rectangle rect = new Rectangle();
                rect.Margin = new Thickness(x, y, 0, 0);
                rect.VerticalAlignment = VerticalAlignment.Top;
                rect.HorizontalAlignment = HorizontalAlignment.Left;
                rect.Width = rectWidth;
                rect.Height = rectHeight;
                rect.Stroke = Brushes.Black;
                //MessageBox.Show("Clicked!");
                byte red = (byte)rnd.Next(256);
                byte blue = (byte)rnd.Next(256);
                byte green = (byte)rnd.Next(256);
                SolidColorBrush brush = new SolidColorBrush(Color.FromRgb(blue, red, green));
                rect.Fill = brush;


                rect.MouseDown += Click_Handler;
                grid1.Children.Add(rect);
            }
        }

        private void Click_Handler(object sender, MouseButtonEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            //MessageBox.Show("Clicked!");
            //byte red = (byte)rnd.Next(256);
            //byte blue = (byte)rnd.Next(256);
            //byte green = (byte)rnd.Next(256);
            //SolidColorBrush brush = new SolidColorBrush(Color.FromRgb(blue, red, green));
            //rect.Fill = brush;
            if (isOverlapped((Rectangle)sender)== false)
            {
                grid1.Children.Remove((UIElement)sender);
            }
          
        }

        private bool isOverlapped(Rectangle sender)
        {
            int isender = grid1.Children.IndexOf((UIElement)sender);
            for(int i = isender+1; i < grid1.Children.Count; i++)
            {
                Rect send = new Rect(sender.Margin.Left, sender.Margin.Top, Width, Height);
                Rectangle rChild = (Rectangle)grid1.Children[i];
                Rect child = new Rect(rChild.Margin.Left, rChild.Margin.Right, rChild.Width, rChild.Height);
                if (send.IntersectsWith(child))
                    return true;
            }
            return false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void grid1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // MessageBox.Show("click!");
           // createRectangles(2);

        }

        private void grid1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Space:
                    createRectangles(4);
                    break;

                case Key.Delete:
                    grid1.Children.Clear();


                    break;
                 
        }
           
        }
    }
}
