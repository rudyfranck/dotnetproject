﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms_and_WPF_drawing
{
    public partial class Form1 : Form
    {
        Rectangle[] arr = new Rectangle[1000];
        int rCount = 0;
        public Form1()
        {
            InitializeComponent();
            Rectangle rect = new Rectangle(100, 150, 300, 200);
            arr[rCount++] = rect;

            rect = new Rectangle(500, 250, 100, 200);
            arr[rCount++] = rect;

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
           
            Graphics g = this.CreateGraphics();
            Random rnd = new Random();

            for (int i = 0; i < rCount; i++)
            {
                int red = rnd.Next(255);
                int green = rnd.Next(255);
                int blue = rnd.Next(255);
                SolidBrush br = new SolidBrush(Color.FromArgb(red, blue, green));
                g.FillRectangle(br, arr[i]);
                g.DrawRectangle(Pens.Black, arr[i]);
            }       
            
        }
    }
}
