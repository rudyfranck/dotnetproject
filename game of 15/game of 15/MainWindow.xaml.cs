﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace game_of_15
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        int[,] P;
        Random random;
       const  int  n = 4;
     
        public MainWindow()
        {
            InitializeComponent();
            random = new Random();
            P = new int[n,n];

            AddButtons();
        }

        private void AddButtons()
        {
           for(int i= 0; i < n * n - 1; i++)
            {
                Button button = new Button();
                Grid.SetRow(button, i / n);
                Grid.SetColumn(button, i % n);

                int num = random.Next(1, n * n);
                while (NumberExists(num))
                    num = random.Next(1, n * n);

                P[i / n, i % n] = num;
                button.Content = num;
                button.FontSize = 40;


                //click!
                button.Click += new RoutedEventHandler(btnClick);
                grid.Children.Add(button);
            
            }
        }

        private void btnClick(object sender, RoutedEventArgs e)
        {
            int num = int.Parse(((Button)sender).Content.ToString());
            int i, j;

            FindButton(num, out i, out j);

            //down
            if (i < n - 1 && P[i + 1, j] == 0)
            {
                P[i + 1, j] = num;
                P[i, j] = 0;
                Grid.SetRow((Button)sender, i + 1);
            }
            //up
            if (i > 0 && P[i - 1, j] == 0)
            {
                P[i - 1, j] = num;
                P[i, j] = 0;
                Grid.SetRow((Button)sender, i - 1);
            }
            //right
            if (j < n - 1 && P[i, j + 1] == 0)
            {
                P[i,j+1] = num;
                P[i, j] = 0;
                Grid.SetColumn((Button)sender, j + 1);
            }
            //left
            if (j > 0 && P[i,j-1] == 0)
            {
                P[i,j-1] = num;
                P[i, j] = 0;
                Grid.SetColumn((Button)sender, j - 1);
            }

            bool gameOver = GameOver();
            if (gameOver)
            {
                MessageBox.Show("you win !!!");
                Close();
            }
        }
        bool GameOver()
        {
            //method returns true if all 15
            for (int i = 0; i < n-1; i++)
            {
                for (int j = 0; j < n-1; j++)
                {
                    int tempmin = P[i, 0]; // min in row
                    int tempminCol = P[0, i];

                    if (P[i, j] < tempmin && P[i, j] < tempminCol)
                        return true;
                   else
                        
                       return false;
                    
                        
                }
            }
            return false;
        }

        private void FindButton(int num, out int ibut, out int jbut)
        {
            ibut = jbut = -1;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    if (P[i, j] == num)
                    {
                        ibut = i;
                        jbut = j;
                        return;
                    }
        }

        private bool NumberExists(int num)
        {
            //check num is alreray generatred
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    if (P[i, j] == num)
                        return true;
            return false;
        }
    }
}
