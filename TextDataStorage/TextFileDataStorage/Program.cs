﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TextFileDataStorage
{
	class Program
	{
		static void Main(string[] args)
		{
			User user1 = new User
			{
				Name = "subliminal",
				Year = 2013,
				Artist = "maitre gims",
                NumberOfTracks = 49
            };
			User user2 = new User
			{
                Name = "Gentleman 2.0",
                Year = 2017,
                Artist = "dadju",
                NumberOfTracks = 29
            };
            User user3 = new User
            {
                Name = "c'est la loi",
                Year = 2018,
                Artist = " NAZA",
                NumberOfTracks = 20
            };
            User user4 = new User
            {
                Name = "Commando",
                Year = 2017,
                Artist = "niska",
                NumberOfTracks = 18
            };

            const string fileName = "users.txt";
			string dir = Directory.GetCurrentDirectory() +
				"\\Data\\";

			bool ok = true;
			List<User> users = new List<User> { user1, user2, user3, user4 };
			List<User> users1 = new List<User>();

			try
			{
				ok = SaveUsersToFile(users, dir + fileName);
				Console.WriteLine("File" + dir + fileName + " is saved"); 
				ok = ReadUsersFromFile(users1, dir + fileName);
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				return;  
			}

			Console.WriteLine("File " + dir + fileName + "is loaded.");
			foreach (User user in users1)
				Console.WriteLine(user.ToString());

		}

		private static bool ReadUsersFromFile(List<User> users1,
			string fileName)
		{
			if (!File.Exists(fileName))
				throw new Exception("File " + fileName + " was not found.");

			users1.Clear();
			StreamReader reader = File.OpenText(fileName);

			
			User newUser = User.ReadFromFile(reader);
			while (newUser != null)
			{
				users1.Add(newUser);
				newUser = User.ReadFromFile(reader);
			}	

			reader.Close();
			return true;
		}

		private static bool SaveUsersToFile(List<User> users, string fileName)
		{
			int index = fileName.IndexOf("\\");
			int lastIndex =0;
			while (index > -1)
			{
				lastIndex = index;
				index = fileName.IndexOf("\\", index+1);
			}
			string path = fileName.Substring(0, lastIndex); 
			if(!Directory.Exists(path))
			{
				throw new Exception("File " + fileName + " was not created");
			}
			StreamWriter writer = File.CreateText(fileName);
			foreach (User user in users)
				user.SaveToFile(writer);
			writer.Close();
			return true;
		}
	}
}
