﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TextFileDataStorage
{
	class User
	{
		public User(string line)
		{
			try
			{
				char[] delim = { '(', ')' };
				string[] s = line.Split(delim);
				Name = s[1];
				Year = int.Parse(s[3]);
				Artist = s[5];
                NumberOfTracks = int.Parse(s[3]);
            }
			catch(Exception e)
			{
				throw new Exception("File content is corrputed!!");
			}
		}

		public User()
		{
			Name = Artist = "";
			Year= NumberOfTracks= 0;
		}

		public string Name { get; set; }
		public int Year { get; set; }
		public string Artist { get; set; }
        public int NumberOfTracks { get; set; }

		internal void SaveToFile(StreamWriter writer)
		{
			String sName = "Name = " + Name;
			String sYear = "Year = " + Year.ToString();
			String sArtist = "Artist = " + Artist;
            String sNumberOfTrack = "NUmberOfTracks = " + NumberOfTracks.ToString();
            writer.WriteLine(sName + " " + sYear + " " + sArtist + " " + sNumberOfTrack);
				
		}

		public override string ToString()
		{
            String sName = "Name = " + Name;
            String sYear = "Year = " + Year.ToString();
            String sArtist = "Artist = " + Artist;
            String sNumberOfTrack = "NUmberOfTracks = " + NumberOfTracks.ToString();
            return sName + " " + sYear + " " + sArtist + " " + sNumberOfTrack;
        }

		internal static User ReadFromFile(StreamReader reader)
		{
			string line = reader.ReadLine();
			if (line != null)
			{
				return new User(line);
			}
			else
				return null;
		}
	}
}
