﻿using System;
using System.Collections.Generic;
using System.Xml;
using XMLDataStorage;
using XMLDataStorsongs;

namespace XMLDataStorsongs
{
    class Program
    {
        static void Main(string[] args)
        {
            AddMusiciansToXML("musicians.xml");
            XmlDocument doc = new XmlDocument();
            List<Musician> musicians = LoadMusiciansXML("musicians.xml");
            foreach (Musician u in musicians)
                Console.WriteLine(u.ToString());
            XPathSearch("musicians.xml");
                }
        private static void XPathSearch(string filiName)
        {
          
            XmlDocument doc = new XmlDocument();
            doc.Load(filiName);
            XmlElement xRoot = doc.DocumentElement;
            
            XmlNodeList childNodes = xRoot.SelectNodes("*");
            foreach (XmlNode node in childNodes)
                Console.WriteLine(node.OuterXml);
            
            childNodes = xRoot.SelectNodes("musician");
            foreach (XmlNode node in childNodes)
                Console.WriteLine(node.SelectSingleNode("@name").Value);
            
            XmlNode nodeBen = xRoot.SelectSingleNode("musician[@name='Mukaririsean']");
            if (nodeBen != null)
                Console.WriteLine(nodeBen.OuterXml);

           
        }
        private static void AddMusiciansToXML(string filiName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filiName);
            XmlElement xRoot = doc.DocumentElement;
            
            XmlElement musiElem = doc.CreateElement("musician");
           
            XmlAttribute nameAttr = doc.CreateAttribute("name");
            
            XmlElement albumelem = doc.CreateElement("album");
            XmlElement songselem = doc.CreateElement("songs");
            
            XmlText nameText = doc.CreateTextNode("c'est la loi");
            XmlText compText = doc.CreateTextNode("NAZA");
            XmlText songsText = doc.CreateTextNode("20");
            //Adding
            nameAttr.AppendChild(nameText);//name
            albumelem.AppendChild(compText);//album
            songselem.AppendChild(songsText);//songs
            //append attribute
            musiElem.Attributes.Append(nameAttr);
            //appending child node
            musiElem.AppendChild(albumelem);
            musiElem.AppendChild(songselem);
            //append to root
            xRoot.AppendChild(musiElem);
            doc.Save(filiName);
        }
        private static List<Musician> LoadMusiciansXML(String filiName)
        {
            List<Musician> musicians = new List<Musician>();
            XmlDocument doc = new XmlDocument();
            doc.Load(filiName);

            XmlElement xRoot = doc.DocumentElement;
            foreach (XmlNode node in xRoot)
            {
                Musician musician = new Musician();
                if (node.Attributes.Count > 0)
                {
                    XmlNode attr = node.Attributes.GetNamedItem("name");
                    if (attr != null)
                        musician.Name = attr.Value;

                }
                foreach (XmlNode child in node.ChildNodes)
                {
                    switch (child.Name)
                    {
                        case "album":
                            musician.Album = child.InnerText;
                            break;
                        case "songs":
                            musician.Songs = Int32.Parse(child.InnerText);
                            break;


                    }
                }
                musicians.Add(musician);
            }
            return musicians;
        }
    }
}
