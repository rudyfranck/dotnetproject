﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Animation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer;
        public MainWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            CreateAnimation();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // CreateAnimation();
            timer.Stop();
        }

        private void CreateAnimation()
        {
            DoubleAnimation a = new DoubleAnimation();
            a.From = btn1.ActualWidth;
            a.To = 500;
            a.Duration = new Duration(TimeSpan.FromSeconds(1));
            a.AutoReverse = true;
          //  a.BeginTime = TimeSpan.FromSeconds(1);

            Storyboard.SetTarget(a, btn1);
            Storyboard.SetTargetProperty(a, new PropertyPath(Button.WidthProperty));
            Storyboard animation = new Storyboard();
            animation.Children.Add(a);
            animation.Begin();

        }
    }
}
