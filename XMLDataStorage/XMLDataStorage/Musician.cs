﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace XMLDataStorage
{
    class Musician
    {

        public Musician(string line)
        {
            try
            {
                char[] delim = { '(', ')' };
                string[] s = line.Split(delim);
                Name = s[1];
                Songs = int.Parse(s[3]);
                Album = s[5];
            }
            catch (Exception e) 
            {
                throw new Exception("File content is corrupted!!");
            }
        }
        public Musician()
        {
            Name = Album = "";
            Songs = 0;
        }
        public String Name { get; set; }
        public int Songs { get; set; }
        public string Album { get; set; }

        internal void SaveToFile(StreamWriter writer)
        {
            string sName = "Name : (" + Name+")";
            string sSongs = "Songs : (" + Songs.ToString()+")";
            string sAlbum = "Album : (" + Album +")";
            writer.WriteLine(sName + " " + sSongs + " " + sAlbum);
        }

        public override string ToString()
        {
            string sName = "Name :" + Name + " ,";
            string sSongs = "Songs : " + Songs.ToString()+" ,";
            string sAlbum = "Album : " + Album ;
           return sName + " " + sSongs + " " + sAlbum;
        }

        internal static Musician ReadFromFile(StreamReader reader)
        {
            string line = reader.ReadLine();
            if(line !=null)
            {
                return new Musician(line);
            }
            else
            return null;
        }
    }
}
