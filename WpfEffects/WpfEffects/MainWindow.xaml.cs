﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfEffects
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        SolidColorBrush colorBrush = new SolidColorBrush(Color.FromRgb(255, 0, 0));//red brush

        //  GradientBrush br = not to be created
        LinearGradientBrush lineargradient = new LinearGradientBrush();

        public MainWindow()
        {
            InitializeComponent();
           // btn1.Background = colorBrush;
        }
    }
}
